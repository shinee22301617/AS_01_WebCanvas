let style = 1; //pen style
let a = 0; //click or not
let width = 25;
let colorr=0;
let colorg=0;
let colorb=0;
let beginx=0;
let beginy=0;
var undostack =new Stack();
var redostack =new Stack();
var shapelog ;


function showbegin(event) {
    a = 1;
    var x = event.clientX;
    var y = event.clientY;
    var coords = "Begin : (" + x + "," + y + ")";
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    document.getElementById("demo").innerText = coords;
    undostack.push(ctx.getImageData(0,0,1000,600));
    redostack.clear();

    if(style==3)
    {
      beginx=x;
      beginy=y;
      document.getElementById('txt').style.visibility = "visible";
      document.getElementById('txt').style.top = y+"px";
      document.getElementById('txt').style.left = x+"px";
    }
    else if(style==4 || style==5 ||style==6)
    {
      shapelog = ctx.getImageData(0,0,1000,600);
      beginx=x;
      beginy=y;
    }
    else
    {
      draw(x,y);
    }
}

document.addEventListener('keydown', function(e){  
  var keyID = e.code;
  if (keyID === "Enter")  {
    document.getElementById('txt').style.visibility = "hidden";
    write(document.getElementById('txt').value);
    document.getElementById('txt').value = "";
  }
}, false);

function showend(event) {
    a = 0;
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    var x = event.clientX;
    var y = event.clientY;
    var coords = "End : (" + x + "," + y + ")";
    document.getElementById("demo").innerText = coords;
    
}

function showmove(event) {
    var x = event.clientX;
    var y = event.clientY;
    if(style==4 || style==5 || style==6)
    {
      shape(x,y);
    }
    else
    {
      draw(x,y);
    }
}

function showbegin1(event) {
  a = 1;
  var x = event.clientX;
  var y = event.clientY;
  var coords = "Begin : (" + x + "," + y + ")";
  //document.getElementById("demo").innerText = coords +"s";
}

function showend1(event) {
  a = 0;
  var x = event.clientX;
  var y = event.clientY;
  var coords = "End : (" + x + "," + y + ")";
  colorr = x-1050;
  colorg = y-30;
  //document.getElementById("demo").innerText = coords +"s";
  document.getElementById("demo").innerText = colorr +" "+ colorg+" "+colorb;
  document.getElementById("colorshow").style.backgroundColor = 'rgb(' + colorr + ',' + colorg + ','+ colorb +')';
}

function showmove1(event) {
  var x = event.clientX;
  var y = event.clientY;
  var c = document.getElementById("three");
  var ctx = c.getContext("2d");
  if(a==1)
    {
      if(x<=1306 && x>=1050 && y>=30 && y<=286)
      {
        for (var i=0;i<256;i++){
          ctx.fillStyle = 'rgb(' + (x-1050) + ',' + (y-30) + ','+ i +')';
          ctx.fillRect(0,i,50,1);
      }
    }
  }
}

function showbegin2(event) {
  a = 1;
  var x = event.clientX;
  var y = event.clientY;
  var coords = "Begin : (" + x + "," + y + ")";
  //document.getElementById("demo").innerText = coords +"x";
}

function showend2(event) {
  a = 0;
  var x = event.clientX;
  var y = event.clientY;
  var coords = "End : (" + x + "," + y + ")";
  colorb = y-30;
  document.getElementById("demo").innerText = colorr +" "+ colorg+" "+colorb;
  document.getElementById("colorshow").style.backgroundColor = 'rgb(' + colorr + ',' + colorg + ','+ colorb +')';
}

function showmove2(event) {
  var x = event.clientX;
  var y = event.clientY;
  var c = document.getElementById("small");
  var ctx = c.getContext("2d");
  if(a==1)
  {
      if(x>=1320 && x<=1370 && y>=30 && y<=286)
      {
        for (var i=0;i<256;i++){
          for (var j=0;j<256;j++){
            ctx.fillStyle = 'rgb(' + i + ',' + j + ','+ (y-30) +')';
            ctx.fillRect(i,j,1,1);
          }
      }
    }
  }
}

function draw(x,y)
{
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    if(style==1)
    {
        if(a==0)
        {
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(x,y);
            ctx.lineTo(x,y);
            ctx.stroke();
        }
        else
        {
            //游標設定準確
            ctx.lineWidth = width;
            //設定linewidth
            ctx.lineTo(x,y);
            ctx.strokeStyle = 'rgb(' + colorr + ',' + colorg + ','+ colorb +')';
            //顏色
            ctx.stroke();
        }
    }
    else if(style==2)
    {
        if(a==0)
        {
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(x,y);
            ctx.lineTo(x,y);
            ctx.stroke();
        }
        else
        {
          ctx.globalCompositeOperation = 'destination-out'  
          //游標設定準確
            ctx.lineWidth = width;
            //設定linewidth
            ctx.lineTo(x,y);
            ctx.strokeStyle = 'rgb(' + colorr + ',' + colorg + ','+ colorb +')';
            //顏色
            ctx.stroke();
            ctx.globalCompositeOperation = 'source-over' 
        }
    }
}

function shape(x,y) {
  if(a==1)
    {
      var c = document.getElementById("myCanvas");
      var ctx = c.getContext("2d");
      ctx.putImageData(shapelog,0,0);
      if(style==4)
      {
        ctx.beginPath();
        ctx.strokeStyle = 'rgb(' + colorr + ',' + colorg + ','+ colorb +')';
        ctx.lineWidth = 5;
        ctx.moveTo(beginx,beginy);
        ctx.strokeRect(x, y , beginx-x, beginy-y);
        ctx.stroke();
      }
      else if(style==5)
      {
        ctx.beginPath();
        ctx.lineWidth = 5;
        ctx.strokeStyle = 'rgb(' + colorr + ',' + colorg + ','+ colorb +')'
        ctx.moveTo(beginx,beginy);
        ctx.lineTo(x,y);
        ctx.lineTo(2*beginx-x,y);
        ctx.lineTo(beginx,beginy);
        ctx.stroke();
      }
      else if(style==6)
      {
        var x1=Math.pow(x-beginx, 2);
        var y1=Math.pow(y-beginy, 2);
        ctx.lineWidth = 5;
        ctx.strokeStyle = 'rgb(' + colorr + ',' + colorg + ','+ colorb +')'
        var r = Math.pow(x1+y1,0.5);
        ctx.beginPath();
        ctx.arc(beginx,beginy,r,0,2*Math.PI,1);
        ctx.stroke();
      }
    }
}

function write(a){
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.font =document.getElementById("fontsize").value+" "+document.getElementById("font").value;

    ctx.fillStyle = 'rgb(' + colorr + ',' + colorg + ','+ colorb +')';
    ctx.fillText(a, beginx, beginy);
}

function returnpx(x) {
  document.getElementById("demo").innerText =x+'px';
  width = x;
}

function brush(){
  returnbtn();
  document.getElementById('demo').innerText = 'Brush';
  document.getElementById('myCanvas').style.cursor = "url('img/pen.png')15 50, auto";
  style = 1;
  document.getElementById("brush").style.backgroundColor = "#e6ff78";
}

function eraser() {
  returnbtn();
  document.getElementById('demo').innerText = 'Eraser';
  document.getElementById('myCanvas').style.cursor = "url('img/eraser.png')35 30, auto";
  style = 2;
  document.getElementById("eraser").style.backgroundColor = "#e6ff78";

  var c = document.getElementById("myCanvas");
  var ctx = c.getContext("2d");
}

function text(){
  returnbtn();
  document.getElementById('myCanvas').style.cursor = "text";
  document.getElementById("demo").innerText = 'text';
  style = 3;
  document.getElementById("text").style.backgroundColor = "#e6ff78";
}

function undo() {
  document.getElementById("demo").innerText = 'undo';
  var c = document.getElementById("myCanvas");
  var ctx = myCanvas.getContext("2d");
  if(!undostack.isEmpty())
  {
    redostack.push(ctx.getImageData(0,0,1000,600));
    c.height = undostack.peek().height;
    c.width = undostack.peek().width;
    ctx.putImageData(undostack.peek(),0,0);
    undostack.pop();
    
  }
}

function redo() {
  document.getElementById("demo").innerText = 'redo';
  var c = document.getElementById("myCanvas");
  var ctx = myCanvas.getContext("2d");
  if(!redostack.isEmpty())
  {
    undostack.push(ctx.getImageData(0,0,1000,600));
    c.height = undostack.peek().height;
    c.width = undostack.peek().width;
    ctx.putImageData(redostack.peek(),0,0);
    redostack.pop();
  }
}

function rectangle() {
  returnbtn();
  document.getElementById("demo").innerText = 'rectangle';
  document.getElementById('myCanvas').style.cursor = "crosshair";
  style = 4;
  document.getElementById("rectangle").style.backgroundColor = "#e6ff78";
}

function triangle() {
  returnbtn();
  document.getElementById("demo").innerText = 'triangle';
  document.getElementById('myCanvas').style.cursor = "crosshair";
  style = 5;
  document.getElementById("triangle").style.backgroundColor = "#e6ff78";
}

function circle() {
  returnbtn();
  document.getElementById("demo").innerText = 'circle';
  document.getElementById('myCanvas').style.cursor = "crosshair";
  style = 6;
  document.getElementById("circle").style.backgroundColor = "#e6ff78";
}

function reset() {
  document.getElementById("demo").innerText = 'Reset';
  var c = document.getElementById("myCanvas");
  var ctx = c.getContext("2d");
  ctx.clearRect(0, 0, c.width, c.height);
}

function download(){
  var c = document.getElementById("myCanvas");
  var image = c.toDataURL();
  var DownloadLink = document.createElement('a');
  DownloadLink.download = 'canvas_image.png';
  DownloadLink.href = image;
  DownloadLink.click();
  document.getElementById('demo').innerText = 'Download';
}

function returnbtn() {
  if(style==1)
  {
      document.getElementById('brush').style.backgroundColor = "white";
  }
  else if (style==2)
  {
      document.getElementById('eraser').style.backgroundColor = "white";
  }
  else if(style==3)
  {
      document.getElementById('text').style.backgroundColor = "white";
      document.getElementById('txt').style.visibility = "hidden";
      document.getElementById('txt').value = "";
      a=0;
  }
  else if(style==4)
  {
    document.getElementById('rectangle').style.backgroundColor = "white";
  }
  else if(style==5)
  {
    document.getElementById('triangle').style.backgroundColor = "white";
  }
  else if(style==6)
  {
    document.getElementById('circle').style.backgroundColor = "white";
  }
}

function expandfont() {
    document.getElementById("demo").innerText =  document.getElementById('serif').value;
    document.getElementById('serif').style.visibility = "visible";
    document.getElementById('sans-serif').style.visibility = "visible";
    document.getElementById('cursive').style.visibility = "visible";
    document.getElementById('fantasy').style.visibility = "visible";
}

function setfont(input) {
  document.getElementById('font').value = input;
  document.getElementById('font').style.fontFamily = input;
  document.getElementById('serif').style.visibility = "hidden";
  document.getElementById('sans-serif').style.visibility = "hidden";
  document.getElementById('cursive').style.visibility = "hidden";
  document.getElementById('fantasy').style.visibility = "hidden";
}

function changeback(value,color) {
  document.getElementById(value).style.backgroundColor =color;
}

function expandsize() {
  document.getElementById("demo").innerText =  document.getElementById('serif').value;
  document.getElementById('font12').style.visibility = "visible";
  document.getElementById('font24').style.visibility = "visible";
  document.getElementById('font36').style.visibility = "visible";
  document.getElementById('font48').style.visibility = "visible";
}

function setsize(input) {
  document.getElementById('fontsize').value = input;
  document.getElementById('font12').style.visibility = "hidden";
  document.getElementById('font24').style.visibility = "hidden";
  document.getElementById('font36').style.visibility = "hidden";
  document.getElementById('font48').style.visibility = "hidden";
}

function upload() {
  document.getElementById('labelupload').click();
  document.getElementById('demo').innerText = "upload"
}

function Stack() {
  var items = [];
  this.push = function(element) {
   items.push(element);
  }
  this.pop = function() {
   return items.pop();
  }
  this.peek = function() {
   return items[items.length - 1];
  }
  this.isEmpty = function() {
   return items.length === 0;
  }
  this.clear = function() {
   items = [];
  }
  this.size = function() {
   return items.length;
  }
}

